package mx.tecnm.misantla.formularioitics

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var animation = AnimationUtils.loadAnimation(this,R.anim.animacion)
        ImagenLogo.startAnimation(animation)
        tvMensaje.startAnimation(animation)

        var handler = Handler().postDelayed({
            startActivity(Intent(this,MainActivity2::class.java))
        },5000)

    }
}

